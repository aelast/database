<?php

namespace Aelast\Database;

use \Illuminate\Database\Capsule\Manager as Capsule;

class Database
{

    private static $capsule;

    public static function init(Array $config)
    {
        static::$capsule = new Capsule;
        static::$capsule->addConnection($config);
        static::$capsule->bootEloquent();
    }

}
